<?php
/**
 * Sahana security library error defines
 *
 * PHP version 4 and 5
 *
 * LICENSE: This source file is subject to LGPL license
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/copyleft/lesser.html
 *
 * @package    framework
 * @subpackage security
 * @author     Ravindra De Silva <ravindra@opensource.lk><ravidesilva@iee.org>
 * @copyright  Lanka Software Foundation - http://www.opensource.lk
 * @license    http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License (LGPL)
 * 
 */

define(SHN_ERR_OR_NAME_INCOMPLETE,"Organization name is essential for registration. Please Complete and Submit again");
define(SHN_ERR_OR_TYPE_INCOMPLETE,"Organization Type is essential for registration. Please Complete and Submit again");
define(SHN_ERR_OR_SECTOR_INCOMPLETE,"Organization Sector is essential for registration. Please Complete and Submit again");
define(SHN_ERR_OR_LOCATION_INCOMPLETE,"Organization Location is essential for registration. Please Complete and Submit again");
define(SHN_ERR_OR_UNAME_INCOMPLETE,"User Name is essential for registration. Please Complete and Submit again");
define(SHN_ERR_OR_PWD_INCOMPLETE,"Password is essential for registration. Please Complete and Submit again");
define(SHN_ERR_OR_REPWD_INCOMPLETE,"Password confirmation is essential for registration. Please Complete and Submit again");
define(SHN_ERR_OR_NAME_INCOMPLETE,"Organization name is essential for registration. Please Complete and Submit again");
define(SHN_ERR_OR_ADD_OPTION,"Choose group or individual enrty.");
define(SHN_ERR_OR_ADD_TYPE,"Select the type of the entry");
define(SHN_ERR_OR_PREFER,"Select the preference of form display");
?>
